FROM cern/cc7-base:20201201-1.x86_64

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>

RUN yum -y install sssd && \
      yum clean all && \
      rm -rf /var/cache/yum

CMD ["/bin/echo", "This container is not configured.\nHelp at https://gitlab.cern.ch/sciencebox/docker-images/sssd"]
