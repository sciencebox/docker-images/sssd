# sssd

Docker image to provide the SSSD service.


### Usage
- The image only comes with the software;
- Configuration files for `sssd` are not provided in the Docker image;
- It should be used in combination with Helm charts (or similar tools for container configuration) to create the expected configuration files

#### Example usage
- Via Helm charts provide a configuration file at `/etc/sssd/sssd.conf`. It should be owned by root and readable by root only;
- Start the sssd service overriding the default command with `command: ["/usr/sbin/sssd", "--config=/etc/sssd/sssd.conf", "--interactive"]`


### Exposing SSSD to other contianer
- Other containers with `sssd-client` installed can use the service provided by this container;
- To do so, it is needed to export `/var/lib/sss` as volume;
- Other containers can mount it with the option `--volumes-from <sssd_server_name>`.
